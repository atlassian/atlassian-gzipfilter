package it.com.atlassian.gzipfilter;

import it.com.atlassian.gzipfilter.IntegrationTestUtils.HttpHeader;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Optional;
import java.util.zip.GZIPInputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test to ensure that an out.flush() in a servlet results in bytes being send to the browser
 */
public class TestFlushing
{
    @Test
    public void testFlushingWithNoFilter() throws Exception {
        doTimingTest(null, null);
    }
    @Test
    public void testFlushingWithGzFilter() throws Exception {
        doTimingTest(IntegrationTestUtils.GZIP_RESPONSE_HEADER, IntegrationTestUtils.GZIP_ACCEPT_HEADER);
    }

    private void doTimingTest(HttpHeader expectedContentEncoding, HttpHeader acceptHeader) throws IOException, InterruptedException {

        long pause = 4000;
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest.Builder builder = HttpRequest.newBuilder();
        builder.uri(URI.create(IntegrationTestUtils.URL + "flushing.html?delay=" + pause));
        if (acceptHeader != null) {
            builder.header(acceptHeader.getKey(), acceptHeader.getValue());
        }

        HttpRequest request = builder.build();

        long t0 = System.currentTimeMillis();
        HttpResponse<InputStream> response = client.send(request, BodyHandlers.ofInputStream());
        long t1 = System.currentTimeMillis();

        assertEquals(200, response.statusCode(),
                "Should return 200 - success");

        Optional<String> contentEncoding = response.headers().firstValue("Content-Encoding");
        if (expectedContentEncoding != null) {
            assertTrue(contentEncoding.isPresent(),
                    "Content-Encoding is not set or empty");
            HttpHeader responseContentEncoding = HttpHeader.of("Content-Encoding", contentEncoding.get());
            assertEquals(expectedContentEncoding, responseContentEncoding);
        } else {
            assertTrue(contentEncoding.isEmpty(),
                    "Content-Encoding must be empty");
        }

        InputStream inputStream = response.body();
        if (acceptHeader != null) {
            inputStream = new GZIPInputStream(inputStream, 1);
        }

        expect("first", inputStream);
        long t2 = System.currentTimeMillis();
        expect("second", inputStream);
        long t3 = System.currentTimeMillis();
        expect("last", inputStream);
        long t4 = System.currentTimeMillis();

        // should get "first" within 100ms of making the request
        assertTrue((t2 - t0) < 100,
                "read 'first' in response took: " + (t2 - t0));
        assertTimeWithin(pause, t2, t3); // should get "second" within 4000 of "first"
        assertTimeWithin(pause, t3, t4); // should get "last" within 4000 of "second"
    }

    private static void assertTimeWithin(long expectedDelay, long t0, long t1) {
        assertEquals(expectedDelay, t1 - t0, 80);
    }

    private static void expect(String expected, InputStream in) throws IOException {
        for (int i = 0; i < expected.length(); i++) {
            int a = expected.charAt(i);
            int b = in.read();
            assertEquals(a, b);
        }
    }
}
