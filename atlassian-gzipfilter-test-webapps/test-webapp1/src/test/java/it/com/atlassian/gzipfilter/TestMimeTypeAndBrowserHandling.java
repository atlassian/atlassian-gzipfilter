package it.com.atlassian.gzipfilter;

import com.atlassian.gzipfilter.test.web.UTF8Servlet;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static it.com.atlassian.gzipfilter.IntegrationTestUtils.GZIP_ACCEPT_HEADER;
import static it.com.atlassian.gzipfilter.IntegrationTestUtils.assertNonGzipped;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMimeTypeAndBrowserHandling
{
    @Test
    public void testWebappDoesNotGzipWhenClientDoesNotSupportIt() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(
                URI.create(IntegrationTestUtils.URL + "test.html")).build();

        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        assertEquals(200, response.statusCode(),
                "Should return 200 - success");
        assertNonGzipped(response);
    }

    @Test
    public void testWebappDoesGzipAndContentIsCorrect() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(
                URI.create(IntegrationTestUtils.URL + "test.html")).build();

        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        assertEquals(200, response.statusCode(),
                "Should return 200 - success");
        assertNonGzipped(response);

        String expected = new String(gzipContent("<h1>Test Servlet</h1>"), StandardCharsets.UTF_8);
        String actual  = new String(gzipContent(response.body()), StandardCharsets.UTF_8);
        assertEquals(expected, actual);
    }

    @Test
    public void testWebappOnlyGzipsCertainMimeTypes() throws IOException, InterruptedException {
        // test mime-types that should be gzipped
        assertGzipped("text/plain", true);
        assertGzipped("text/html", true);
        assertGzipped("application/javascript", true);
        assertGzipped("application/xhtml+xml", true);
        assertGzipped("application/xml", true);

        // test mime-types that shouldn't be gzipped
        assertGzipped("application/octet-stream", false);
        assertGzipped("image/png", false);
        assertGzipped("image/jpeg", false);
        assertGzipped("image/jpg", false);
    }

    @Test
    public void testWebappOnlyGzipsCertainMimeTypesForCertainBrowsers() throws IOException, InterruptedException {
        final String IE_6 = "Mozilla/4.0 (compatible; MSIE 6.0b; Windows NT 5.1)";
        final String FIREFOX_3 = "Mozilla/6.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:2.0.0.0) Gecko/20061028 Firefox/3.0";

        assertGzipped("text/css", IE_6, false);
        assertGzipped("text/html", IE_6, false);
        assertGzipped("text/javascript", IE_6, false);

        assertGzipped("text/css", FIREFOX_3, true);
        assertGzipped("text/html", FIREFOX_3, true);
        assertGzipped("text/javascript", FIREFOX_3, true);
    }

    @Test
    public void testUTF8() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(IntegrationTestUtils.URL + "utf8.html"))
                .header(GZIP_ACCEPT_HEADER.getKey(), GZIP_ACCEPT_HEADER.getValue())
                .build();

        HttpResponse<byte[]> response =  client.send(request, BodyHandlers.ofByteArray());
        assertEquals(200, response.statusCode(),
                "Should return 200 - success");
        IntegrationTestUtils.assertGzipped(response);

        String expectedText = "<h1>Test Servlet: '" + UTF8Servlet.UTF8_TEXT + "' </h1>";
        String actualText = gunzipContent(response.body(), StandardCharsets.UTF_8);
        assertEquals(expectedText, actualText);
    }

    private void assertGzipped(String mimeType, boolean shouldGzip) throws IOException, InterruptedException {
        assertGzipped(mimeType, null, shouldGzip);
    }

    private void assertGzipped(final String mimeType, final String userAgent, final boolean shouldGzip)
            throws IOException, InterruptedException {
        IntegrationTestUtils.assertPathGzipped(
                "test.html?mimetype=" + URLEncoder.encode(mimeType, StandardCharsets.UTF_8),
                userAgent,
                shouldGzip);
    }

    private byte[] gzipContent(String content) throws IOException
    {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        GZIPOutputStream outputStream = new GZIPOutputStream(bao);
        outputStream.write(content.getBytes(StandardCharsets.UTF_8));
        outputStream.close();
        return bao.toByteArray();
    }

    private String gunzipContent(byte[] content, Charset charset) throws IOException
    {
        ByteArrayInputStream bai = new ByteArrayInputStream(content);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        GZIPInputStream inputStream = new GZIPInputStream(bai);
        int i;
        while ((i = inputStream.read()) != -1)
        {
            bao.write(i);
        }
        return bao.toString(charset);
    }
}
