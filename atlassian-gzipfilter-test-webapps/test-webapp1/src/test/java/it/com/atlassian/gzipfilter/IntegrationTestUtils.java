package it.com.atlassian.gzipfilter;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * An utility class to facilitate testing
 */
class IntegrationTestUtils
{
    static final String HTTP_PORT = System.getProperty("http.port", "8080");
    static final String URL = "http://localhost:" + HTTP_PORT + "/test-webapp1/";

    static final HttpHeader GZIP_RESPONSE_HEADER = HttpHeader.of("Content-Encoding", "gzip");
    static final HttpHeader GZIP_ACCEPT_HEADER = HttpHeader.of("Accept-Encoding", "gzip");

    static HttpResponse<String> assertPathGzipped(
            String path,
            String userAgent,
            boolean shouldGzip) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();

        List<String> headers = new ArrayList<>(
                asList(GZIP_ACCEPT_HEADER.getKey(), GZIP_ACCEPT_HEADER.getValue()));
        if (userAgent != null) {
            headers.addAll(asList("User-Agent", userAgent));
        }

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(URL + path))
                .headers(headers.toArray(new String[0]))
                .GET()
                .build();

        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        assertEquals(200, response.statusCode(),
                "Should return 200 - success");
        if (shouldGzip)
            assertGzipped(response);
        else
            assertNonGzipped(response);

        return response;
    }

    static <T> void assertGzipped(HttpResponse<T> response)
    {
        HttpHeaders headers = response.headers();
        assertEquals(
                headers.firstValue(GZIP_RESPONSE_HEADER.getKey()),
                Optional.of(GZIP_RESPONSE_HEADER.getValue()),
                "Should return gzipped content");
    }

    static <T> void assertNonGzipped(HttpResponse<T> response)
    {
        HttpHeaders headers = response.headers();
        Optional<String> contentEncoding = headers.firstValue(GZIP_RESPONSE_HEADER.getKey());
        contentEncoding.ifPresent(
                value -> fail(GZIP_RESPONSE_HEADER.getKey() + " should be empty but was " + value));
    }

    public static class HttpHeader extends AbstractMap.SimpleEntry<String, String> {
        public static HttpHeader of(String key, String value) {
            return new HttpHeader(key, value);
        }

        public HttpHeader(String key, String value) {
            super(key, value);
        }

        public HttpHeader(HttpHeader header) {
            super(header);
        }
    }
}
