package it.com.atlassian.gzipfilter;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import static it.com.atlassian.gzipfilter.IntegrationTestUtils.GZIP_ACCEPT_HEADER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class TestResponseTypes
{
    @Test
    public void test304HasNoBody() throws IOException, InterruptedException {
        HttpResponse<String> response = executeMethodAcceptingGzip("304.html");
        assertEquals(304, response.statusCode(),
                "Should return 304 - not modified");
        assertEmptyBody(response);
    }

    @Test
    public void testRedirect() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newBuilder()
                .followRedirects(Redirect.NEVER)
                .build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(IntegrationTestUtils.URL + "redirect.html"))
                .header(GZIP_ACCEPT_HEADER.getKey(), GZIP_ACCEPT_HEADER.getValue())
                .build();

        // test mime-type of image/png is not gzipped
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        assertEquals(302, response.statusCode(),
                "Should return 302 - redirect");

        // According to the HTTP spec, redirect should have a body:
        //   http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
        // "Unless the request method was HEAD, the entity of the response
        // SHOULD contain a short hypertext note with a hyperlink to the new URI(s)"
//        assertEmptyBody(method);
    }

    @Test
    public void testUrlRewriteClassesAreNotFound() throws IOException, InterruptedException {
        HttpResponse<String> response = executeMethodAcceptingGzip("classes.html");
        assertEquals(200, response.statusCode(),
                "Should return 200 if classes can not be found");
    }

    private static void assertEmptyBody(HttpResponse<String> response)
    {
        String bodyString =  response.body();
        if (bodyString != null && bodyString.length() > 0)
            fail("Content should be null, but was '" + bodyString + "'");
    }

    private HttpResponse<String> executeMethodAcceptingGzip(String urlPath) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(IntegrationTestUtils.URL + urlPath))
                .header(GZIP_ACCEPT_HEADER.getKey(), GZIP_ACCEPT_HEADER.getValue())
                .build();
        return client.send(request, BodyHandlers.ofString());
    }
}
