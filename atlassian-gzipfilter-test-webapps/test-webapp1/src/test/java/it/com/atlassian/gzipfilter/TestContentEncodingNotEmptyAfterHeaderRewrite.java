package it.com.atlassian.gzipfilter;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.net.URLEncoder;
import java.net.http.HttpHeaders;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import static it.com.atlassian.gzipfilter.IntegrationTestUtils.assertPathGzipped;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestContentEncodingNotEmptyAfterHeaderRewrite
{
    private static List<Arguments> mimeTypes() {
        return List.of(
                Arguments.of(List.of("application/octet-stream", "application/javascript"), true),
                Arguments.of(List.of("application/javascript","application/octet-stream"), false),
                Arguments.of(List.of("text/html;charset=UTF-8","application/atom+xml"), false)
        );
    }

    public void testRewriteWith(
            String path,
            String outputMethod,
            List<String> mimeTypesSequence,
            boolean gzipped) throws IOException, InterruptedException {

        String mimetypes = String.join(",", mimeTypesSequence);
        String urlPath = path + "?outputMethod=" + outputMethod +
                "&mimetypes=" + URLEncoder.encode(mimetypes, StandardCharsets.UTF_8);

        HttpResponse<String> response = assertPathGzipped(urlPath, null, gzipped);

        String expectedType = mimeTypesSequence.get(mimeTypesSequence.size() - 1);
        HttpHeaders headers = response.headers();
        Optional<String> contentTypeHeader = headers.firstValue("Content-Type");

        assertTrue(contentTypeHeader.isPresent(),
                "Content-Type header is empty");
        assertTrue(contentTypeHeader.get().startsWith(expectedType));
    }

    @ParameterizedTest
    @MethodSource("mimeTypes")
    public void testRewritePrintWriter(List<String> mimeTypesSequence,
                                       boolean gzipped) throws IOException, InterruptedException {
        testRewriteWith("rewriter.html", "PrintWriter",
                mimeTypesSequence, gzipped);
    }

    @ParameterizedTest
    @MethodSource("mimeTypes")
    public void testRewriteOutputStream(List<String> mimeTypesSequence,
                                        boolean gzipped) throws IOException, InterruptedException {
        testRewriteWith("rewriter.html", "OutputStream",
                mimeTypesSequence, gzipped);
    }

    @ParameterizedTest
    @MethodSource("mimeTypes")
    public void testRewriteFilterPrintWriter(List<String> mimeTypesSequence,
                                             boolean gzipped) throws IOException, InterruptedException {
        testRewriteWith("rewriter/noop.html", "PrintWriter",
                mimeTypesSequence, gzipped);
    }

    @ParameterizedTest
    @MethodSource("mimeTypes")
    public void testRewriteFilterOutputStream(List<String> mimeTypesSequence,
                                              boolean gzipped) throws IOException, InterruptedException {
        testRewriteWith("rewriter/noop.html", "OutputStream",
                mimeTypesSequence, gzipped);
    }

    @ParameterizedTest
    @MethodSource("mimeTypes")
    public void testIncludeRewriterPrintWriter(List<String> mimeTypesSequence,
                                               boolean gzipped) throws IOException, InterruptedException {
        testRewriteWith("include_rewriter.html", "PrintWriter",
                mimeTypesSequence, gzipped);
    }

    @ParameterizedTest
    @MethodSource("mimeTypes")
    public void testIncludeRewriterOutputStream(List<String> mimeTypesSequence,
                                                boolean gzipped) throws IOException, InterruptedException {
        testRewriteWith("include_rewriter.html", "OutputStream",
                mimeTypesSequence, gzipped);
    }

    @Disabled("This is how JIRA now uses the filter, and it's failing")
    @ParameterizedTest
    @MethodSource("mimeTypes")
    public void testBrokenIncludeRewriterPrintWriter(List<String> mimeTypesSequence,
                                                     boolean gzipped) throws IOException, InterruptedException {
        testRewriteWith("broken/include_rewriter.html", "PrintWriter",
                mimeTypesSequence, gzipped);
    }

    @Disabled("This is how JIRA now uses the filter, and it's failing")
    @ParameterizedTest
    @MethodSource("mimeTypes")
    public void testBrokenIncludeRewriterOutputStream(List<String> mimeTypesSequence,
                                                      boolean gzipped) throws IOException, InterruptedException {
        testRewriteWith("broken/include_rewriter.html", "OutputStream",
                mimeTypesSequence, gzipped);
    }
}
