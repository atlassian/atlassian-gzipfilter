package com.atlassian.gzipfilter.test.web;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * A noop filter that wraps request and response
 */
public class NoopFilter implements Filter
{
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {}


    @Override
    public void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain filterChain)
            throws IOException, ServletException
    {
        filterChain.doFilter(
                new HttpServletRequestWrapper((HttpServletRequest)req),
                new HttpServletResponseWrapper((HttpServletResponse)resp)
        );
    }

    @Override
    public void destroy() {}
}
