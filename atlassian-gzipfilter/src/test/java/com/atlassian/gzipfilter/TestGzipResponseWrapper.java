package com.atlassian.gzipfilter;

import org.junit.jupiter.api.Test;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestGzipResponseWrapper
{
    @Test
    public void testCreateValidGzipStreamEvenWhenNothingWrittenToResponse() throws Exception
    {
        final HttpServletResponse mockHttpResponse = mock(HttpServletResponse.class);
        final ByteArrayServletOutputStream byteOutputStream = new ByteArrayServletOutputStream();
        when(mockHttpResponse.getOutputStream())
                .thenReturn(byteOutputStream);

        final GzipResponseWrapper gzipResponseWrapper = new GzipResponseWrapper(mockHttpResponse, "UTF-8");

        gzipResponseWrapper.finishResponse();

        //verify an empty but valid gzip stream was constructed
        final byte[] content = byteOutputStream.getContent();
        final GZIPInputStream gzipInputStream = new GZIPInputStream(new ByteArrayInputStream(content));
        assertEquals(-1, gzipInputStream.read());
    }

    @Test
    public void testGzipStreamClosesQuietly() throws IOException {
        final ServletOutputStream mockOutputStream = mock(ServletOutputStream.class);
        doThrow(NullPointerException.class)
                .when(mockOutputStream).close();

        final HttpServletResponse mockHttpResponse = mock(HttpServletResponse.class);
        when(mockHttpResponse.getOutputStream())
                .thenReturn(mockOutputStream);

        final GzipResponseWrapper gzipResponseWrapper =
                new GzipResponseWrapper(mockHttpResponse, "UTF-8");

        assertDoesNotThrow(gzipResponseWrapper::finishResponse);
    }

    @Test
    public void testGzipStreamThrowsUnexpectedException() throws IOException {
        final ServletOutputStream mockOutputStream = mock(ServletOutputStream.class);
        doThrow(NullPointerException.class)
                .when(mockOutputStream).flush();

        final HttpServletResponse mockHttpResponse = mock(HttpServletResponse.class);
        when(mockHttpResponse.getOutputStream())
                .thenReturn(mockOutputStream);

        final GzipResponseWrapper gzipResponseWrapper =
                new GzipResponseWrapper(mockHttpResponse, "UTF-8");

        assertNotNull(gzipResponseWrapper.getOutputStream());   // create stream
        assertThrows(IOException.class, gzipResponseWrapper::flushBuffer);
    }

    static class ByteArrayServletOutputStream extends ServletOutputStream
    {
        private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        @Override
        public void write(final int b) throws IOException
        {
            outputStream.write(b);
        }

        byte[] getContent()
        {
            return outputStream.toByteArray();
        }
    }
}
