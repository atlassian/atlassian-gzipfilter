package com.atlassian.gzipfilter;

import com.atlassian.gzipfilter.selector.GzipCompatibilitySelector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * <p>This Class currently only tests that the content-encoding header is not set when a redirect/error occurs.</p>
 * <p>todo: Write tests for most of the functionality.</p>
 */
@ExtendWith(MockitoExtension.class)
public class TestSelectingResponseWrapper
{
    @Mock
    private GzipCompatibilitySelector selector;

    @BeforeEach
    public void setUp()
    {
        when(selector.shouldGzip(anyString()))
                .thenAnswer((invocationOnMock) ->
                        invocationOnMock.getArgument(0).equals("text/html"));
    }

    @Test
    public void testNoZipOnSpecificContent() throws IOException
    {
        HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");

        selectingResponseWrapper.setContentType("image/png");
        assertNull(wrappedResponse.getHeader("content-encoding"));
        selectingResponseWrapper.sendRedirect("string");
        selectingResponseWrapper.finishResponse();
        assertNull(wrappedResponse.getHeader("content-encoding"));
        assertTrue(wrappedResponse.isSendRedirectCalled());
    }

    @Test
    public void testNoZipOnRedirect() throws IOException
    {
        HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");

        selectingResponseWrapper.setContentType("text/html");
        assertNull(wrappedResponse.getHeader("content-encoding"));
        selectingResponseWrapper.sendRedirect("string");
        selectingResponseWrapper.finishResponse();
        assertNull(wrappedResponse.getHeader("content-encoding"));
        assertTrue(wrappedResponse.isSendRedirectCalled());
    }

    @Test
    public void testNoZipOnError() throws IOException
    {
        HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");

        selectingResponseWrapper.setContentType("text/html");
        assertNull(wrappedResponse.getHeader("content-encoding"));
        selectingResponseWrapper.sendError(404);
        selectingResponseWrapper.finishResponse();
        assertNull(wrappedResponse.getHeader("content-encoding"));
        assertTrue(wrappedResponse.isSendErrorCalled());
    }

    /**
     * Tests that the response is not gzipped when the response code is {@link HttpServletResponse#SC_NO_CONTENT}
     */
    @Test
    public void testNoZipOnNoContentResponse()
    {
        final HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");
        selectingResponseWrapper.setContentType("text/html");

        assertNull(wrappedResponse.getHeader("content-encoding"));
        selectingResponseWrapper.setStatus(HttpServletResponse.SC_NO_CONTENT);
        selectingResponseWrapper.finishResponse();
        assertNull(wrappedResponse.getHeader("content-encoding"));
    }

    /**
     * Tests that the response is not gzipped when the response code is {@link HttpServletResponse#SC_NOT_MODIFIED}
     */
    @Test
    public void testNoZipOnNotModifiedResponse()
    {
        final HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");
        selectingResponseWrapper.setContentType("text/html");

        assertNull(wrappedResponse.getHeader("content-encoding"));
        selectingResponseWrapper.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
        selectingResponseWrapper.finishResponse();
        assertNull(wrappedResponse.getHeader("content-encoding"));
    }

    @Test
    public void testNoActivateGzipWhenContentLengthHasBeenSetAlready()
    {
        final HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        wrappedResponse.setHeader("Content-Length", "123");
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");
        selectingResponseWrapper.setContentType("text/html");
        selectingResponseWrapper.finishResponse();

        assertNull(wrappedResponse.getHeader("content-encoding"),
                "If Content-Length header was set, gzip should not be activated");
    }

    private static class HeaderTrackingResponse extends HttpServletResponseWrapper
    {
        private final Map<String, String> headersSet = new HashMap<>();
        private boolean sendRedirectCalled = false;
        private boolean sendErrorCalled = false;

        public HeaderTrackingResponse() {
            super(mock(HttpServletResponse.class));
        }

        public boolean containsHeader(String key)
        {
            return headersSet.containsKey(key.toLowerCase());
        }

        public void setHeader(String s, String s1)
        {
            super.setHeader(s, s1);
            headersSet.put(s.toLowerCase(), s1);
        }

        public void addHeader(String s, String s1)
        {
            super.setHeader(s, s1);
            String existingHeader = (String) headersSet.get(s.toLowerCase());
            headersSet.put(s.toLowerCase(), existingHeader + "," + s1);
        }

        public String getHeader(String key)
        {
            return (String) headersSet.get(key.toLowerCase());
        }

        public void sendRedirect(String s) throws IOException
        {
            super.sendRedirect(s);
            sendRedirectCalled = true;
        }

        public void sendError(int anErrorCode, String anErrorMessage) throws IOException
        {
            super.sendError(anErrorCode, anErrorMessage);
            sendErrorCalled = true;
        }

        public void sendError(int anErrorCode) throws IOException
        {
            super.sendError(anErrorCode);
            sendErrorCalled = true;
        }

        public void setDateHeader(String string, long l)
        {
            // this is not implemented by Mocks, so just ignore, otherwise you get exceptions in the logs :)
        }

        public boolean isSendRedirectCalled()
        {
            return sendRedirectCalled;
        }

        boolean isSendErrorCalled()
        {
            return sendErrorCalled;
        }
    }
}
