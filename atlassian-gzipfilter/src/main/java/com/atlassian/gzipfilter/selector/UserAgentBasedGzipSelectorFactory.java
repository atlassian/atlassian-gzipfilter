package com.atlassian.gzipfilter.selector;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableSet;

/**
 * This factory will lok into user agent header to decide whenever to dogzip compressing or not
 * <p/>
 * By default gzip is on for defaultCompressableMimeTypes mime types and off for noCompressionUserAgents (IE 6 only)
 * <p/>
 * This is adjustable via COMPRESSABLE_MIME_TYPES_PARAM_NAME an NO_COMPRESSION_USER_AGENTS_PARAM_NAME init-params
 * respectively
 */
public class UserAgentBasedGzipSelectorFactory implements GzipCompatibilitySelectorFactory
{

    public static final String COMPRESSABLE_MIME_TYPES_PARAM_NAME = "compressableMimeTypes";

    public static final String NO_COMPRESSION_USER_AGENTS_PARAM_NAME = "noCompressionUserAgents";

    public static final String USER_AGENT_HEADER = "User-Agent";

    private static final GzipCompatibilitySelector NO_GZIP_SELECTOR = new NoGzipCompatibilitySelector();

    public static final Set<String> DEFAULT_COMPRESSABLE_MIME_TYPES = unmodifiableSet(new HashSet<String>(asList(
                            "text/html",
                            "text/xml",
                            "text/plain",
                            "text/javascript",
                            "text/css",
                            "application/javascript",
                            "application/x-javascript",
                            "application/xml",
                            "application/xhtml",
                            "application/xhtml+xml",
                            "application/json")));

    private static final List<String> DEFAULT_NO_COMPRESSION_USER_AGENTS = unmodifiableList(asList("MSIE 6"));

    private final Set<String> compressableMimeTypes;
    private final List<String> noCompressionUserAgents;

    public UserAgentBasedGzipSelectorFactory(FilterConfig filterConfig)
    {
        final String compressableMimeTypesString = filterConfig.getInitParameter(COMPRESSABLE_MIME_TYPES_PARAM_NAME);
        compressableMimeTypes = compressableMimeTypesString != null
                ? unmodifiableSet(new HashSet<String>(split(compressableMimeTypesString)))
                : DEFAULT_COMPRESSABLE_MIME_TYPES;

        final String noCompressionUserAgentsString = filterConfig.getInitParameter(NO_COMPRESSION_USER_AGENTS_PARAM_NAME);
        noCompressionUserAgents = noCompressionUserAgentsString != null
                ? unmodifiableList(split(noCompressionUserAgentsString))
                : DEFAULT_NO_COMPRESSION_USER_AGENTS;

    }

    public GzipCompatibilitySelector getSelector(final FilterConfig filterConfig, final HttpServletRequest request)
    {
        final String userAgentHeader = request.getHeader(USER_AGENT_HEADER);
        if (userAgentHeader != null)
        {
            for (String noCompressionUserAgent : noCompressionUserAgents)
            {
                if (userAgentHeader.contains(noCompressionUserAgent))
                {
                    return NO_GZIP_SELECTOR;
                }
            }
        }
        return new MimeTypeBasedSelector(compressableMimeTypes);
    }

    private List<String> split(String string)
    {
        return asList(string.split(","));
    }
}
