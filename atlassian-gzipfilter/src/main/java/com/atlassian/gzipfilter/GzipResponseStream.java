package com.atlassian.gzipfilter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.zip.GZIPOutputStream;

public final class GzipResponseStream extends ServletOutputStream
{
    final GZIPOutputStream gzipstream;
    final HttpServletResponse response;

    private final static int DEFAULT_BUFFER_SIZE_BYTES = 1024;

    private static <T> void invoke(Callable<T> method) throws IOException {
        try {
            method.call();
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    public GzipResponseStream(HttpServletResponse response) throws IOException
    {
        this.response = response;
        this.gzipstream = new GZIPOutputStream(
                response.getOutputStream(),
                DEFAULT_BUFFER_SIZE_BYTES,
                true);
    }

    public void close() throws IOException
    {
        invoke(() -> { gzipstream.close(); return null; });
    }

    public void flush() throws IOException
    {
        invoke(() -> { gzipstream.flush(); return null; });
    }

    public void write(int b) throws IOException
    {
        invoke(() -> { gzipstream.write(b); return null; });
    }

    public void write(byte b[]) throws IOException
    {
        this.write(b, 0, b.length);
    }

    public void write(byte b[], int off, int len) throws IOException
    {
        invoke(() -> { gzipstream.write(b, off, len); return null; });
    }
}
