package com.atlassian.gzipfilter.integration;

import javax.servlet.http.HttpServletRequest;

/**
 * This interface is the way that applications integrate with the {@link com.atlassian.gzipfilter.GzipFilter}.
 * <p/>
 * By using an interface instead of an abstract class, we avoid the
 * <a href="http://en.wikipedia.org/wiki/Fragile_base_class">Fragile Base Class</a> problem.
 */
public interface GzipFilterIntegration
{
    /**
     *
     * @return  Whether this application has gzip enabled or not
     */
    boolean useGzip();

    /**
     * What response encoding to use.  This is primarily used when handling the parsing of
     * writers to streams and vice-versa.  Usually this is UTF-8, but depending on your application
     * this may be different.
     *
     * @param request   If you wish to parse the request for the request encoding
     * @return          An encoding supported by java
     */
    String getResponseEncoding(HttpServletRequest request);

}
